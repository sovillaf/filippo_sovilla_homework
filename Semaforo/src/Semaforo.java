public class Semaforo extends Thread  //estendo thread che possiede il metodo run()
{
	int stato, r=3, v=2, g=1;
	
	public void run()
	{
		while(true)
		{
			try { //try e catch identificano, nel caso in cui ci siano,
					//gli errori in esecuzione
				setStato(r);
				Thread.sleep(r*1000);
				setStato(v);
				Thread.sleep(v*1000);
				setStato(g);
				Thread.sleep(g*1000);
			}catch (InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void setStato(int stato)
	{
		this.stato=stato;
		System.out.println(getName() + " : "+stato);
	}
	
}